import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:worky/models/enums/user_type.dart';
import 'package:worky/models/schedule.dart';
import 'package:worky/models/user.dart';
import 'package:worky/models/work_preferences.dart';
import 'package:worky/pages/calendar_page.dart';
import 'package:worky/pages/home_page.dart';
import 'package:worky/pages/profile_page.dart';
import 'package:worky/services/generic_service_offline.dart';
import 'package:worky/services/ioc_container.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

void main() async {
  await Hive.initFlutter();
  await IoCContainer.setup();
  await setGuestUser();
  runApp(const MyApp());
}

Future<void> setGuestUser() async {
  final userService = GetIt.instance.get<GenericServiceOffline<User>>();
  if (userService
      .getItems()
      .where((element) => element.userType == UserType.guest)
      .isEmpty) {
    User user = User(
      userType: UserType.guest,
      workPreferences: WorkPreferences(
        createdAt: DateTime.now(),
        schedule: Schedule(),
      ),
      createdAt: DateTime.now(),
      name: 'Guest',
      email: '',
    );
    user.id = 0;
    await userService.add(user, 0);
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Worky',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      builder: (context, child) => MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child!),
      home: PersistentTabView(
        controller: PersistentTabController(initialIndex: 1),
        navBarStyle: NavBarStyle.style13,
        popAllScreensOnTapAnyTabs: true,
        context,
        screens: [
          const CalendarPage(),
          HomePage(),
          const ProfilePage(),
        ],
        items: [
          PersistentBottomNavBarItem(
            icon: const Icon(Icons.calendar_today),
            title: "Calendar",
            activeColorPrimary: Colors.deepPurple,
            inactiveColorPrimary: Colors.grey,
          ),
          PersistentBottomNavBarItem(
            icon: const Icon(Icons.home),
            title: "Home",
            activeColorPrimary: Colors.deepPurple,
            inactiveColorPrimary: Colors.grey,
          ),
          PersistentBottomNavBarItem(
            icon: const Icon(Icons.person),
            title: "Profile",
            activeColorPrimary: Colors.deepPurple,
            inactiveColorPrimary: Colors.grey,
          ),
        ],
      ),
    );
  }
}
