import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:worky/models/enums/entry_type.dart';
import 'package:worky/models/schedule.dart';
import 'package:worky/models/user.dart';
import 'package:worky/pages/entry_page.dart';
import 'package:worky/services/generic_service_offline.dart';
import 'package:worky/utils/extensions/datetime_extentions.dart';
import 'package:worky/utils/extensions/duration_extensions.dart';
import 'package:worky/utils/metrics.dart';
import 'package:worky/utils/utils.dart';
import 'package:worky/widgets/titled_divider.dart';

import '../models/entry.dart';

class CalendarCalendarPage extends StatefulWidget {
  final entryService = GetIt.instance.get<GenericServiceOffline<Entry>>();
  final userService = GetIt.instance.get<GenericServiceOffline<User>>();
  final DateTime initialMonthDate;
  CalendarCalendarPage({super.key, initialMonthDate})
      : initialMonthDate = initialMonthDate ??
            DateTime(DateTime.now().year, DateTime.now().month, 1);

  @override
  State<CalendarCalendarPage> createState() => _CalendarCalendarPageState();
}

class _CalendarCalendarPageState extends State<CalendarCalendarPage> {
  DateTime? _selectedDate;
  @override
  Widget build(BuildContext context) {
    final user = widget.userService.getItem(0);
    return StreamBuilder(
        stream: widget.entryService.listStream,
        builder: (context, snapshot) {
          final entries = snapshot.data ?? widget.entryService.getItems();
          return Column(
            children: [
              buildCalendar(context, entries),
              const TitledDivider(title: "Metrics"),
              user != null
                  ? StreamBuilder(
                      stream: widget.userService.listStream,
                      builder: (context, snapshot) => _buildMetrics(
                          user.workPreferences.schedule,
                          widget.initialMonthDate))
                  : const Text("N/A"),
            ],
          );
        });
  }

  Widget buildCalendar(BuildContext context, List<Entry> entries) {
    TableCalendar calendar = TableCalendar(
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.none,
      calendarBuilders: CalendarBuilders(
          todayBuilder: (context, day, focusedDay) =>
              _buildTodayColorCells(day, entries),
          selectedBuilder: (context, day, focusedDay) =>
              _buildEntryColorCells(day, entries),
          defaultBuilder: (context, day, focusedDay) =>
              _buildEntryColorCells(day, entries)),
      headerVisible: false,
      headerStyle:
          const HeaderStyle(formatButtonVisible: false, titleCentered: true),
      selectedDayPredicate: (day) => isSameDay(_selectedDate, day),
      focusedDay: widget.initialMonthDate,
      firstDay: DateTime.utc(DateTime.now().year - 100, 1, 1),
      lastDay: DateTime.utc(DateTime.now().year + 100, 12, 31),
      onDaySelected: (selectedDay, focusedDay) {
        setState(() {
          _selectedDate = selectedDay;
        });
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => EntryPage(
                    selectedDate: selectedDay,
                  )),
        );
      },
    );
    return calendar;
  }

  Widget _buildEntryColorCells(DateTime day, List<Entry> entries) {
    bool entryOnDay = entries.any((Entry e) => e.start.isSameDate(day));
    return Container(
      margin: const EdgeInsets.all(5),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: entryOnDay ? Colors.lightGreen : Colors.transparent,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Text(
        day.day.toString(),
      ),
    );
  }

  Widget _buildTodayColorCells(DateTime day, List<Entry> entries) {
    bool entryOnDay = entries.any((Entry e) => e.start.isSameDate(day));
    return Container(
      margin: const EdgeInsets.all(5),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: entryOnDay ? Colors.lightGreen : Colors.blueAccent.shade100,
        borderRadius: BorderRadius.circular(20),
        border: entryOnDay
            ? Border.all(width: 5, color: Colors.blueAccent.shade100)
            : null,
      ),
      child: Text(
        day.day.toString(),
      ),
    );
  }

  Widget _buildMetrics(Schedule schedule, DateTime date) {
    var metrics = Metrics(userId: 0);
    var workDaysInMonth =
        metrics.scheduledWorkTimeInMonth(date: date).toDoubleMinutes() /
            schedule.workTime.toDoubleMinutes();
    var workedTimeInMonth = metrics
        .workedTimeInRange(
            from: Utils.getDaysInMonth(date: date).first,
            to: Utils.getDaysInMonth(date: date).last)
        .toDoubleMinutes();
    var workTimeInWeek =
        metrics.scheduledWorkTimeInWeek(date: date).toDoubleMinutes();
    var workedTimeInWeek = metrics
        .workedTimeInRange(
            from: Utils.getDaysInWeek(date: date).first,
            to: Utils.getDaysInWeek().last)
        .toDoubleMinutes();
    var absenceDays = widget.entryService
        .getItems()
        .where((entry) =>
            entry.start.month == date.month &&
            entry.entryType != EntryType.onPremise &&
            entry.entryType != EntryType.homeOffice)
        .length;

    return ListView(
      shrinkWrap: true,
      children: [
        ListTile(
          title: const Text('Total hours'),
          trailing: Text(workedTimeInMonth.toString()),
        ),
        ListTile(
          title: const Text('Work days'),
          trailing: Text(workDaysInMonth.toString()),
        ),
        ListTile(
          title: const Text('Absence'),
          trailing: Text(absenceDays.toString()),
        ),
        ListTile(
          title: const Text('This week'),
          trailing: Text("$workedTimeInWeek/$workTimeInWeek"),
        ),
      ],
    );
  }
}
