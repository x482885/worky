import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';

class CalendarTileSwitch extends StatelessWidget {
  final ValueChanged<bool> onValueChanged;
  final bool isTileSwitched;

  const CalendarTileSwitch({
    super.key,
    required this.isTileSwitched,
    required this.onValueChanged,
  });

  @override
  Widget build(BuildContext context) {
    int isTileSwitchedIndex = isTileSwitched ? 1 : 0;
    return SizedBox.fromSize(
      size: const Size(90, 40),
      child: ToggleSwitch(
        initialLabelIndex: isTileSwitchedIndex,
        totalSwitches: 2,
        icons: const [
          Icons.calendar_today,
          Icons.view_day,
        ],
        onToggle: (index) {
          if (isTileSwitchedIndex == index) return;
          onValueChanged(index == 1);
        },
      ),
    );
  }
}
