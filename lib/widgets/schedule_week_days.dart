import 'package:flutter/material.dart';

class ScheduleWeekDays extends StatefulWidget {
  final List<String> weekDays = [
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun'
  ];
  final List<bool> inputWorkingDays;
  final bool editable;
  final ValueChanged onValueChanged;
  ScheduleWeekDays(
      {super.key,
      required this.inputWorkingDays,
      this.editable = false,
      required this.onValueChanged});

  @override
  State<ScheduleWeekDays> createState() => _ScheduleWeekDaysState();
}

class _ScheduleWeekDaysState extends State<ScheduleWeekDays> {
  List<bool> workingDays = [];

  @override
  Widget build(BuildContext context) {
    workingDays = widget.inputWorkingDays;
    return Row(
      children: [
        for (var i = 0; i < widget.weekDays.length; i++)
          Expanded(
            child: Column(
              children: [
                Text(widget.weekDays[i]),
                Checkbox(
                    value: workingDays[i],
                    onChanged: widget.editable
                        ? (value) {
                            setState(() {
                              workingDays[i] = value!;
                              widget.onValueChanged(workingDays);
                            });
                          }
                        : null)
              ],
            ),
          ),
      ],
    );
  }
}
