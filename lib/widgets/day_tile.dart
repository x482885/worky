import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:worky/utils/extensions/string_extensions.dart';

import '../models/entry.dart';
import '../pages/entry_page.dart';

class DayTile extends StatelessWidget {
  final DateTime day;
  final Entry? entry;
  const DayTile({super.key, required this.day, this.entry});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => EntryPage(
                    selectedDate: day,
                  )),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          border: Border.all(
            color: entry == null
                ? Colors.grey
                : const Color.fromARGB(255, 85, 85, 85),
          ),
        ),
        child: entry == null ? buildEmptyTile() : buildEntryTile(),
      ),
    );
  }

  Widget buildEmptyTile() {
    return Row(
      children: [
        Text(
          day.day.toString(),
          style: const TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: Colors.grey,
          ),
        ),
        const SizedBox(width: 8),
        Text(
          DateFormat('EEE').format(day),
          style: const TextStyle(
            fontSize: 18,
            color: Colors.grey,
          ),
        ),
      ],
    );
  }

  Widget buildEntryTile() {
    return Row(
      children: [
        Text(
          day.day.toString(),
          style: const TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(width: 8),
        Text(
          DateFormat('EEE').format(day),
          style: const TextStyle(
            fontSize: 18,
          ),
        ),
        const SizedBox(width: 8),
        Expanded(
            flex: 1,
            child: Center(
                child: Text("type: ${entry!.entryType.name.capitalize()}"))),
        Expanded(
          flex: 1,
          child: Center(
            child: Text(
              "total: "
              '${entry!.end.difference(entry!.start).inHours}:${entry!.end.difference(entry!.start).inMinutes.remainder(60).toString().padLeft(2, '0')}',
            ),
          ),
        ),
      ],
    );
  }
}
