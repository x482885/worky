import 'package:flutter/material.dart';

class NotificationTile extends StatelessWidget {
  final String title;
  final String description;
  final VoidCallback onClear;

  const NotificationTile({super.key, 
    required this.title,
    required this.description,
    required this.onClear,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title),
      subtitle: Text(description),
      trailing: IconButton(
        icon: const Icon(Icons.check),
        onPressed: onClear,
      ),
    );
  }
}
