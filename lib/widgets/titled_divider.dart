import 'package:flutter/material.dart';

class TitledDivider extends StatelessWidget {
  final String? title;
  const TitledDivider({super.key, this.title});

  @override
  Widget build(BuildContext context) {
    return title == null
        ? const Divider()
        : Row(
            children: [
              const Expanded(child: Divider()),
              Text(title!),
              const Expanded(child: Divider()),
            ],
          );
  }
}
