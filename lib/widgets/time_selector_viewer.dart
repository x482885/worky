import 'package:flutter/material.dart';

class TimeSelectorViewer extends StatefulWidget {
  final String label;
  final TimeOfDay inputTime;
  final Function(TimeOfDay) onTimeChanged;
  final bool enabled;

  const TimeSelectorViewer({
    super.key,
    required this.label,
    required this.inputTime,
    required this.onTimeChanged,
    this.enabled = true,
  });

  @override
  _TimeSelectorViewerState createState() => _TimeSelectorViewerState();
}

class _TimeSelectorViewerState extends State<TimeSelectorViewer> {
  TimeOfDay time = const TimeOfDay(hour: 0, minute: 0);

  @override
  void initState() {
    super.initState();
    time = widget.inputTime;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text("${widget.label}:"),
        const Spacer(),
        Text(time.format(context)),
        TextButton(
            onPressed: widget.enabled
                ? () async {
                    final newTime = await showTimePicker(
                      context: context,
                      initialTime: time,
                    );

                    if (newTime != null) {
                      widget.onTimeChanged(newTime);
                      setState(() => time = newTime);
                    }
                  }
                : null,
            child: const Text("Set.."))
      ],
    );
  }
}
