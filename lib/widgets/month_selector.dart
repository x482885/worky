import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MonthSelector extends StatelessWidget {
  final DateTime currentMonthDate;
  final ValueChanged<DateTime> onValueChanged;
  MonthSelector(
      {super.key, required this.onValueChanged, DateTime? currentMonthDate})
      : currentMonthDate = currentMonthDate ??
            DateTime(DateTime.now().year, DateTime.now().month, 1);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
            onPressed: () => onValueChanged(DateTime(currentMonthDate.year,
                currentMonthDate.month - 1, currentMonthDate.day)),
            icon: const Icon(Icons.arrow_back)),
        const Spacer(),
        TextButton(
          onPressed: null,
          child: Text(
            DateFormat('yyyy MMMM').format(currentMonthDate),
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        const Spacer(),
        IconButton(
            onPressed: () => onValueChanged(DateTime(currentMonthDate.year,
                currentMonthDate.month + 1, currentMonthDate.day)),
            icon: const Icon(Icons.arrow_forward))
      ],
    );
  }
}
