import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:worky/models/schedule.dart';
import 'package:worky/widgets/monthly_basis_dropdown.dart';
import 'package:worky/widgets/schedule_week_days.dart';
import 'package:worky/widgets/time_selector_viewer.dart';

class ScheduleForm extends StatelessWidget {
  final Schedule _schedule;
  final bool _isEditable;

  final TextEditingController _hoursPerWeekController = TextEditingController();
  final TextEditingController _deductedMealTimeController =
      TextEditingController();
  final TextEditingController _hoursAfterMealTimeDeductedController =
      TextEditingController();
  bool _isMonthlyBasis = true;
  TimeOfDay _workStart = const TimeOfDay(hour: 8, minute: 0);
  TimeOfDay _workEnd = const TimeOfDay(hour: 16, minute: 30);
  List<bool> _workingDays = [];

  ScheduleForm({
    super.key,
    required Schedule schedule,
    required bool isEditable,
  })  : _schedule = schedule,
        _isEditable = isEditable;

  @override
  Widget build(BuildContext context) {
    _hoursPerWeekController.text = _schedule.hoursPerWeek?.toString() ?? "";
    _isMonthlyBasis = _schedule.isMonthlyBasis;
    _workStart = _schedule.workStart;
    _workEnd = _schedule.workEnd;
    _deductedMealTimeController.text =
        _schedule.deductedMealTime?.inMinutes.toString() ?? "";
    _hoursAfterMealTimeDeductedController.text =
        _schedule.hoursAfterMealTimeDeducted?.inMinutes.toString() ?? "";
    _workingDays = _schedule.workingDays;

    return Column(
      children: [
        TextField(
          controller: _hoursPerWeekController,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          decoration: const InputDecoration(
            labelText: 'Hours Per Week',
          ),
          enabled: _isEditable,
        ),
        MonthlyBasisDropdown(
          isMonthlyBasis: _isMonthlyBasis,
          onValueChanged: (value) => _isMonthlyBasis = value,
          enabled: _isEditable,
        ),
        ScheduleWeekDays(
          inputWorkingDays: _workingDays,
          onValueChanged: (value) => _workingDays = value,
          editable: _isEditable,
        ),
        TimeSelectorViewer(
          label: "Work start",
          inputTime: _workStart,
          onTimeChanged: (time) => _workStart = time,
          enabled: _isEditable,
        ),
        TimeSelectorViewer(
          label: "Work end",
          inputTime: _workEnd,
          onTimeChanged: (time) => _workEnd = time,
          enabled: _isEditable,
        ),
        TextField(
          controller: _deductedMealTimeController,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          decoration: const InputDecoration(
            labelText: 'Deducted Meal Time (minutes)',
          ),
          enabled: _isEditable,
        ),
        TextField(
          controller: _hoursAfterMealTimeDeductedController,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          decoration: const InputDecoration(
            labelText: 'Time after which meal time is deducted (hours)',
          ),
          enabled: _isEditable,
        ),
      ],
    );
  }

  Schedule getSchedule() {
    var deductedMealTime = int.tryParse(_deductedMealTimeController.text);
    var hoursAfterMealTimeDeducted =
        int.tryParse(_hoursAfterMealTimeDeductedController.text);

    return Schedule(
      hoursPerWeek: int.tryParse(_hoursPerWeekController.text),
      isMonthlyBasis: _isMonthlyBasis,
      workingDays: _workingDays,
      workStart: _workStart,
      workEnd: _workEnd,
      deductedMealTime:
          deductedMealTime == null ? null : Duration(minutes: deductedMealTime),
      hoursAfterMealTimeDeducted: hoursAfterMealTimeDeducted == null
          ? null
          : Duration(
              hours: int.tryParse(_hoursAfterMealTimeDeductedController.text) ??
                  0),
    );
  }
}

class ScheduleFormController {
  late Schedule Function() getSchedule;
}
