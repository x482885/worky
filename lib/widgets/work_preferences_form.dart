import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:worky/models/work_preferences.dart';

class WorkPreferencesForm extends StatelessWidget {
  final WorkPreferences _workPreferences;
  final bool _isEditable;

  final TextEditingController companyNameController = TextEditingController();
  final TextEditingController monthlySalaryController = TextEditingController();
  final TextEditingController taxRateController = TextEditingController();
  final TextEditingController availableHolidaysController =
      TextEditingController();
  final TextEditingController availableSickDaysController =
      TextEditingController();
  final TextEditingController paydayController = TextEditingController();

  WorkPreferencesForm({
    super.key,
    required WorkPreferences workPreferences,
    required bool isEditable,
  })  : _isEditable = isEditable,
        _workPreferences = workPreferences;

  @override
  Widget build(BuildContext context) {
    companyNameController.text = _workPreferences.companyName ?? "";
    monthlySalaryController.text =
        _workPreferences.monthlySalary?.toString() ?? "";
    taxRateController.text = _workPreferences.taxRate?.toString() ?? "";
    availableHolidaysController.text =
        _workPreferences.availableHolidayDays?.toString() ?? "";
    availableSickDaysController.text =
        _workPreferences.availableSickDays?.toString() ?? "";
    paydayController.text = _workPreferences.paydayDay?.toString() ?? "";
    bool isEditable = _isEditable;

    return Column(
      children: [
        TextField(
          controller: companyNameController,
          keyboardType: TextInputType.name,
          decoration: const InputDecoration(
            labelText: 'Company Name',
          ),
          onChanged: (_) => isEditable = true,
          enabled: isEditable,
        ),
        TextField(
          controller: monthlySalaryController,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          decoration: const InputDecoration(
            labelText: 'Monthly Salary',
          ),
          onChanged: (_) => isEditable = true,
          enabled: isEditable,
        ),
        TextField(
          controller: taxRateController,
          keyboardType: const TextInputType.numberWithOptions(decimal: true),
          inputFormatters: [
            FilteringTextInputFormatter.allow(RegExp(r'^(\d{1,2})(\.\d{0,2})?'))
          ],
          decoration: const InputDecoration(
            labelText: 'Tax Rate %',
          ),
          onChanged: (_) => isEditable = true,
          enabled: isEditable,
        ),
        TextField(
          controller: availableHolidaysController,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          decoration: const InputDecoration(
            labelText: 'Available Holidays',
          ),
          onChanged: (_) => isEditable = true,
          enabled: isEditable,
        ),
        TextField(
          controller: availableSickDaysController,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          decoration: const InputDecoration(
            labelText: 'Available Sick Days',
          ),
          onChanged: (_) => isEditable = true,
          enabled: isEditable,
        ),
        TextField(
          controller: paydayController,
          keyboardType: TextInputType.number,
          inputFormatters: [
            FilteringTextInputFormatter.allow(
                RegExp(r'^([1-9]|[1][0-9]|[2][0-8])$'))
          ],
          decoration: const InputDecoration(
            labelText: "Payday nth of month",
          ), // TODO day vs workday
          onChanged: (_) => isEditable = true,
          enabled: isEditable,
        ),
        // TODO OP to HO ratio
      ],
    );
  }

// Warning not passing changed schedule, has to be assigned in parent
  WorkPreferences getWorkPreferences() {
    return _workPreferences.copyWith(
      companyName: companyNameController.text,
      monthlySalary: double.tryParse(monthlySalaryController.text),
      taxRate: double.tryParse(taxRateController.text),
      availableHolidayDays: int.tryParse(availableHolidaysController.text),
      availableSickDays: int.tryParse(availableSickDaysController.text),
      paydayDay: int.tryParse(paydayController.text),
    );
  }
}

class WorkPreferencesFormController {
  late WorkPreferences Function() getWorkPreferences;
}
