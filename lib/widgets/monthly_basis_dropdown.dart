import 'package:flutter/material.dart';

class MonthlyBasisDropdown extends StatefulWidget {
  final bool isMonthlyBasis;
  final ValueChanged<bool> onValueChanged;
  final bool enabled;
  const MonthlyBasisDropdown(
      {super.key,
      required this.isMonthlyBasis,
      required this.onValueChanged,
      this.enabled = true});

  @override
  _MonthlyBasisDropdownState createState() => _MonthlyBasisDropdownState();
}

class _MonthlyBasisDropdownState extends State<MonthlyBasisDropdown> {
  String _selectedValue = 'monthly';

  @override
  void initState() {
    super.initState();
    _selectedValue = widget.isMonthlyBasis ? 'monthly' : 'weekly';
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      value: _selectedValue,
      onChanged: widget.enabled
          ? (value) {
              setState(() {
                if (value != null) {
                  _selectedValue = value;
                  widget.onValueChanged(_selectedValue == 'monthly');
                }
              });
            }
          : null,
      items: const [
        DropdownMenuItem(
          value: "monthly",
          child: Text("Monthly"),
        ),
        DropdownMenuItem(
          value: "weekly",
          child: Text("Weekly"),
        ),
      ],
      decoration: const InputDecoration(
        labelText: 'Checked:',
      ),
    );
  }
}
