import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:worky/services/generic_service_offline.dart';
import '../models/entry.dart';
import '../utils/utils.dart';
import 'day_tile.dart';

class CalendarTilesPage extends StatefulWidget {
  final entryService = GetIt.instance.get<GenericServiceOffline<Entry>>();
  final DateTime initialMonthDate;
  CalendarTilesPage({super.key, initialMonthDate})
      : initialMonthDate = initialMonthDate ??
            DateTime(DateTime.now().year, DateTime.now().month, 1);

  @override
  State<CalendarTilesPage> createState() => _CalendarTilesPageState();
}

class _CalendarTilesPageState extends State<CalendarTilesPage> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget.entryService.listStream,
      builder: (context, snapshot) {
        final entries = snapshot.data ?? widget.entryService.getItems();
        final daysInMonth = Utils.getDaysInMonth(date: widget.initialMonthDate);
        return Expanded(
          child: GridView.builder(
            padding: const EdgeInsets.all(8),
            scrollDirection: Axis.vertical,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1,
                mainAxisExtent: MediaQuery.of(context).size.width / 6,
                mainAxisSpacing: 8),
            itemCount: daysInMonth.length,
            itemBuilder: (context, index) {
              final day = daysInMonth[index];
              final entry = entries
                  .where((Entry e) =>
                      e.start.day == day.day &&
                      e.start.month == day.month &&
                      e.start.year == day.year)
                  .firstOrNull;
              return DayTile(day: day, entry: entry);
            },
          ),
        );
      },
    );
  }
}
