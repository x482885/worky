import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../models/base_entity.dart';

class GenericServiceOffline<T> {
  final String collectionName;
  late final Box<T> _box;

  GenericServiceOffline(this.collectionName);

  Future<void> initialize() async {
    _box = await Hive.openBox<T>(collectionName);
  }

  Listenable get listenable => _box.listenable();

  Stream<List<T>> get listStream =>
      _box.watch().map((event) => _box.values.toList());

  T? getItem(int id) {
    return _box.get(id);
  }

  List<T> getItems() {
    return _box.values.toList();
  }

  Future<void> update(int id, T t) async {
    _checkIfBaseEntityAndSet(t: t, id: id);
    await _box.put(id, t);
  }

  Future<void> updateAll(Map<int, T> map) async {
    map.forEach((id, t) => _checkIfBaseEntityAndSet(t: t, id: id));
    await _box.putAll(map);
  }

  Future<int> create(T t) async {
    int id = await _box.add(t);
    _checkIfBaseEntityAndSet(t: t, id: id);
    await _box.put(id, t);
    return id;
  }

  Future<List<int>> createAll(List<T> list) async {
    Iterable<int> ids = await _box.addAll(list);
    Iterator<int> iterator = ids.iterator;
    var map = list.asMap().map((_, value) =>
        MapEntry(iterator.moveNext() ? iterator.current : -1, value));
    map.forEach((id, t) => _checkIfBaseEntityAndSet(t: t, id: id));
    await _box.putAll(map);
    return ids.toList();
  }

  Future<void> delete(int id) async {
    await _box.delete(id);
  }

  Future<void> add(T t, int id) async {
    await _box.put(id, t);
  }

  void _checkIfBaseEntityAndSet({required T t, required int id}) {
    if (t is BaseEntity) {
      t.id = id;
      t.modifiedAt = DateTime.now();
    }
  }
}
