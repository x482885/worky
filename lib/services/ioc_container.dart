import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:worky/models/complex%20adapters/duration_adapter.dart';
import 'package:worky/models/entry.dart';
import 'package:worky/models/enums/check_period.dart';
import 'package:worky/models/enums/entry_type.dart';
import 'package:worky/models/enums/user_type.dart';
import 'package:worky/models/event.dart';
import 'package:worky/models/schedule.dart';
import 'package:worky/models/user.dart';
import 'package:worky/models/work_preferences.dart';
import 'package:worky/services/generic_service.dart';
import 'package:worky/services/generic_service_offline.dart';

class IoCContainer {
  static Future<void> setup() async {
    // Register enums
    Hive.registerAdapter<EntryType>(EntryTypeAdapter());
    Hive.registerAdapter<CheckPeriod>(CheckPeriodAdapter());
    Hive.registerAdapter<UserType>(UserTypeAdapter());

    // Register dependent models
    Hive.registerAdapter<WorkPreferences>(WorkPreferencesAdapter());
    Hive.registerAdapter<Schedule>(ScheduleAdapter());

    // Register complex type adapters
    Hive.registerAdapter<TimeOfDay>(TimeOfDayAdapter());
    Hive.registerAdapter<Duration>(DurationAdapter());

    // Register Models
    await registerModel(EntryAdapter(), (data) => data.toJson(),
        (json) => Entry.fromJson(json));
    await registerModel(EventAdapter(), (data) => data.toJson(),
        (json) => Event.fromJson(json));
    await registerModel(
        UserAdapter(), (data) => data.toJson(), (json) => User.fromJson(json));
  }

  static Future<void> registerAdaptersAndService<T>(
      TypeAdapter<T> adapter) async {
    Hive.registerAdapter<T>(adapter);
    var service = GenericServiceOffline<T>(T.toString().toLowerCase());
    await service.initialize();
    GetIt.I.registerSingleton(service);
  }

  static void registerFirebase<T>(
    Map<String, dynamic> Function(T data) toJson,
    T Function(Map<String, dynamic> json) fromJson,
  ) async {
    GetIt.I.registerSingleton(GenericService<T>(
      toJson,
      fromJson,
      "entries",
    ));
  }

  // Enums are not Models therefore not registered as such
  // This is because Firebase doesn't work with serializable enums
  static Future<void> registerModel<T>(
    TypeAdapter<T> adapter,
    Map<String, dynamic> Function(T data) toJson,
    T Function(Map<String, dynamic> json) fromJson,
  ) async {
    await registerAdaptersAndService<T>(adapter);
    //temp disable firebase
    //registerFirebase(toJson, fromJson);
  }
}
