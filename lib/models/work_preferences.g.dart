// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'work_preferences.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$WorkPreferencesCWProxy {
  WorkPreferences createdAt(DateTime? createdAt);

  WorkPreferences companyName(String? companyName);

  WorkPreferences monthlySalary(double? monthlySalary);

  WorkPreferences taxRate(double? taxRate);

  WorkPreferences availableHolidayDays(int? availableHolidayDays);

  WorkPreferences availableSickDays(int? availableSickDays);

  WorkPreferences paydayDay(int? paydayDay);

  WorkPreferences checkPeriod(CheckPeriod? checkPeriod);

  WorkPreferences ratio(double? ratio);

  WorkPreferences schedule(Schedule schedule);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WorkPreferences(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WorkPreferences(...).copyWith(id: 12, name: "My name")
  /// ````
  WorkPreferences call({
    DateTime? createdAt,
    String? companyName,
    double? monthlySalary,
    double? taxRate,
    int? availableHolidayDays,
    int? availableSickDays,
    int? paydayDay,
    CheckPeriod? checkPeriod,
    double? ratio,
    Schedule? schedule,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfWorkPreferences.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfWorkPreferences.copyWith.fieldName(...)`
class _$WorkPreferencesCWProxyImpl implements _$WorkPreferencesCWProxy {
  const _$WorkPreferencesCWProxyImpl(this._value);

  final WorkPreferences _value;

  @override
  WorkPreferences createdAt(DateTime? createdAt) => this(createdAt: createdAt);

  @override
  WorkPreferences companyName(String? companyName) =>
      this(companyName: companyName);

  @override
  WorkPreferences monthlySalary(double? monthlySalary) =>
      this(monthlySalary: monthlySalary);

  @override
  WorkPreferences taxRate(double? taxRate) => this(taxRate: taxRate);

  @override
  WorkPreferences availableHolidayDays(int? availableHolidayDays) =>
      this(availableHolidayDays: availableHolidayDays);

  @override
  WorkPreferences availableSickDays(int? availableSickDays) =>
      this(availableSickDays: availableSickDays);

  @override
  WorkPreferences paydayDay(int? paydayDay) => this(paydayDay: paydayDay);

  @override
  WorkPreferences checkPeriod(CheckPeriod? checkPeriod) =>
      this(checkPeriod: checkPeriod);

  @override
  WorkPreferences ratio(double? ratio) => this(ratio: ratio);

  @override
  WorkPreferences schedule(Schedule schedule) => this(schedule: schedule);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WorkPreferences(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WorkPreferences(...).copyWith(id: 12, name: "My name")
  /// ````
  WorkPreferences call({
    Object? createdAt = const $CopyWithPlaceholder(),
    Object? companyName = const $CopyWithPlaceholder(),
    Object? monthlySalary = const $CopyWithPlaceholder(),
    Object? taxRate = const $CopyWithPlaceholder(),
    Object? availableHolidayDays = const $CopyWithPlaceholder(),
    Object? availableSickDays = const $CopyWithPlaceholder(),
    Object? paydayDay = const $CopyWithPlaceholder(),
    Object? checkPeriod = const $CopyWithPlaceholder(),
    Object? ratio = const $CopyWithPlaceholder(),
    Object? schedule = const $CopyWithPlaceholder(),
  }) {
    return WorkPreferences(
      createdAt: createdAt == const $CopyWithPlaceholder()
          ? _value.createdAt
          // ignore: cast_nullable_to_non_nullable
          : createdAt as DateTime?,
      companyName: companyName == const $CopyWithPlaceholder()
          ? _value.companyName
          // ignore: cast_nullable_to_non_nullable
          : companyName as String?,
      monthlySalary: monthlySalary == const $CopyWithPlaceholder()
          ? _value.monthlySalary
          // ignore: cast_nullable_to_non_nullable
          : monthlySalary as double?,
      taxRate: taxRate == const $CopyWithPlaceholder()
          ? _value.taxRate
          // ignore: cast_nullable_to_non_nullable
          : taxRate as double?,
      availableHolidayDays: availableHolidayDays == const $CopyWithPlaceholder()
          ? _value.availableHolidayDays
          // ignore: cast_nullable_to_non_nullable
          : availableHolidayDays as int?,
      availableSickDays: availableSickDays == const $CopyWithPlaceholder()
          ? _value.availableSickDays
          // ignore: cast_nullable_to_non_nullable
          : availableSickDays as int?,
      paydayDay: paydayDay == const $CopyWithPlaceholder()
          ? _value.paydayDay
          // ignore: cast_nullable_to_non_nullable
          : paydayDay as int?,
      checkPeriod: checkPeriod == const $CopyWithPlaceholder()
          ? _value.checkPeriod
          // ignore: cast_nullable_to_non_nullable
          : checkPeriod as CheckPeriod?,
      ratio: ratio == const $CopyWithPlaceholder()
          ? _value.ratio
          // ignore: cast_nullable_to_non_nullable
          : ratio as double?,
      schedule: schedule == const $CopyWithPlaceholder() || schedule == null
          ? _value.schedule
          // ignore: cast_nullable_to_non_nullable
          : schedule as Schedule,
    );
  }
}

extension $WorkPreferencesCopyWith on WorkPreferences {
  /// Returns a callable class that can be used as follows: `instanceOfWorkPreferences.copyWith(...)` or like so:`instanceOfWorkPreferences.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$WorkPreferencesCWProxy get copyWith => _$WorkPreferencesCWProxyImpl(this);
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class WorkPreferencesAdapter extends TypeAdapter<WorkPreferences> {
  @override
  final int typeId = 6;

  @override
  WorkPreferences read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return WorkPreferences(
      createdAt: fields[1] as DateTime?,
      companyName: fields[3] as String?,
      monthlySalary: fields[4] as double?,
      taxRate: fields[5] as double?,
      availableHolidayDays: fields[6] as int?,
      availableSickDays: fields[7] as int?,
      paydayDay: fields[8] as int?,
      checkPeriod: fields[9] as CheckPeriod?,
      ratio: fields[10] as double?,
      schedule: fields[11] as Schedule,
    )
      ..id = fields[0] as int?
      ..modifiedAt = fields[2] as DateTime;
  }

  @override
  void write(BinaryWriter writer, WorkPreferences obj) {
    writer
      ..writeByte(12)
      ..writeByte(3)
      ..write(obj.companyName)
      ..writeByte(4)
      ..write(obj.monthlySalary)
      ..writeByte(5)
      ..write(obj.taxRate)
      ..writeByte(6)
      ..write(obj.availableHolidayDays)
      ..writeByte(7)
      ..write(obj.availableSickDays)
      ..writeByte(8)
      ..write(obj.paydayDay)
      ..writeByte(9)
      ..write(obj.checkPeriod)
      ..writeByte(10)
      ..write(obj.ratio)
      ..writeByte(11)
      ..write(obj.schedule)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.createdAt)
      ..writeByte(2)
      ..write(obj.modifiedAt);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WorkPreferencesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkPreferences _$WorkPreferencesFromJson(Map<String, dynamic> json) =>
    WorkPreferences(
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      companyName: json['companyName'] as String?,
      monthlySalary: (json['monthlySalary'] as num?)?.toDouble(),
      taxRate: (json['taxRate'] as num?)?.toDouble(),
      availableHolidayDays: (json['availableHolidayDays'] as num?)?.toInt(),
      availableSickDays: (json['availableSickDays'] as num?)?.toInt(),
      paydayDay: (json['paydayDay'] as num?)?.toInt(),
      checkPeriod:
          $enumDecodeNullable(_$CheckPeriodEnumMap, json['checkPeriod']),
      ratio: (json['ratio'] as num?)?.toDouble(),
      schedule: Schedule.fromJson(json['schedule'] as Map<String, dynamic>),
    )
      ..id = (json['id'] as num?)?.toInt()
      ..modifiedAt = DateTime.parse(json['modifiedAt'] as String);

Map<String, dynamic> _$WorkPreferencesToJson(WorkPreferences instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt.toIso8601String(),
      'modifiedAt': instance.modifiedAt.toIso8601String(),
      'companyName': instance.companyName,
      'monthlySalary': instance.monthlySalary,
      'taxRate': instance.taxRate,
      'availableHolidayDays': instance.availableHolidayDays,
      'availableSickDays': instance.availableSickDays,
      'paydayDay': instance.paydayDay,
      'checkPeriod': _$CheckPeriodEnumMap[instance.checkPeriod],
      'ratio': instance.ratio,
      'schedule': instance.schedule,
    };

const _$CheckPeriodEnumMap = {
  CheckPeriod.weekly: 'weekly',
  CheckPeriod.monthly: 'monthly',
  CheckPeriod.yearly: 'yearly',
};
