import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'schedule.g.dart';

@JsonSerializable()
@HiveType(typeId: 8)
@CopyWith()
class Schedule {
  @HiveField(0)
  final int? hoursPerWeek;
  @HiveField(1)
  final bool isMonthlyBasis;
  @HiveField(2)
  final List<bool> workingDays =
      List<bool>.generate(7, (index) => index < 5, growable: false);
  @HiveField(3)
  @JsonKey(
    fromJson: _timeOfDayFromJson,
    toJson: _timeOfDayToJson,
  )
  final TimeOfDay workStart;
  @HiveField(4)
  @JsonKey(
    fromJson: _timeOfDayFromJson,
    toJson: _timeOfDayToJson,
  )
  final TimeOfDay workEnd;
  @HiveField(5)
  final Duration? deductedMealTime;
  @HiveField(6)
  final Duration? hoursAfterMealTimeDeducted;

  Schedule({
    this.hoursPerWeek,
    this.isMonthlyBasis = true,
    List<bool>? workingDays,
    this.workStart = const TimeOfDay(hour: 8, minute: 0),
    this.workEnd = const TimeOfDay(hour: 16, minute: 30),
    this.deductedMealTime,
    this.hoursAfterMealTimeDeducted,
  }) {
    if (workingDays != null) {
      // remove rest if input list is bigger than 7 elements
      if (workingDays.length > this.workingDays.length) {
        workingDays.removeRange(7, workingDays.length);
      }
      this.workingDays.setRange(0, workingDays.length, workingDays);
    }
  }

  factory Schedule.fromJson(Map<String, dynamic> json) =>
      _$ScheduleFromJson(json);

  Map<String, dynamic> toJson() => _$ScheduleToJson(this);

  Duration get workTime {
    var workTime = Duration(
        hours: workEnd.hour - workStart.hour,
        minutes: workEnd.minute - workStart.minute);
    if (hoursAfterMealTimeDeducted != null &&
        deductedMealTime != null &&
        workTime >= workTime) {
      workTime =
          Duration(minutes: workTime.inMinutes - deductedMealTime!.inMinutes);
    }
    return workTime;
  }
}

TimeOfDay _timeOfDayFromJson(String json) {
  final parts = json.split(':');
  return TimeOfDay(
    hour: int.parse(parts[0]),
    minute: int.parse(parts[1]),
  );
}

String _timeOfDayToJson(TimeOfDay timeOfDay) {
  return '${timeOfDay.hour}:${timeOfDay.minute}';
}
