import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:worky/models/base_entity.dart';

part 'event.g.dart';

@JsonSerializable()
@HiveType(typeId: 4)
@CopyWith()
class Event extends BaseEntity {
  @HiveField(3)
  final String title;

  @HiveField(4)
  final String description;

  @HiveField(5)
  final bool isSeen;

  Event(
      {super.createdAt,
      required this.title,
      required this.description,
      this.isSeen = false});

  factory Event.fromJson(Map<String, dynamic> json) => _$EventFromJson(json);

  Map<String, dynamic> toJson() => _$EventToJson(this);
}
