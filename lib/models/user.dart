import 'package:json_annotation/json_annotation.dart';
import 'package:hive/hive.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:worky/models/work_preferences.dart';

import 'base_entity.dart';
import 'enums/user_type.dart';

part 'user.g.dart';

@JsonSerializable()
@HiveType(typeId: 10)
@CopyWith()
class User extends BaseEntity {
  @HiveField(3)
  final UserType userType;

  @HiveField(4)
  final String name;

  @HiveField(5)
  final String email;

  @HiveField(6)
  final double? phoneNumber;

  @HiveField(7)
  final WorkPreferences workPreferences;

  User({
    super.createdAt,
    required this.userType,
    required this.name,
    required this.email,
    this.phoneNumber,
    required this.workPreferences,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
