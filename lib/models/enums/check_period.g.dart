// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_period.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CheckPeriodAdapter extends TypeAdapter<CheckPeriod> {
  @override
  final int typeId = 3;

  @override
  CheckPeriod read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return CheckPeriod.weekly;
      case 1:
        return CheckPeriod.monthly;
      case 2:
        return CheckPeriod.yearly;
      default:
        return CheckPeriod.weekly;
    }
  }

  @override
  void write(BinaryWriter writer, CheckPeriod obj) {
    switch (obj) {
      case CheckPeriod.weekly:
        writer.writeByte(0);
        break;
      case CheckPeriod.monthly:
        writer.writeByte(1);
        break;
      case CheckPeriod.yearly:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckPeriodAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
