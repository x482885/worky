import 'package:hive/hive.dart';

part 'check_period.g.dart';

@HiveType(typeId: 3)
enum CheckPeriod {
  @HiveField(0)
  weekly,

  @HiveField(1)
  monthly,

  @HiveField(2)
  yearly
}
