// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entry_type.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class EntryTypeAdapter extends TypeAdapter<EntryType> {
  @override
  final int typeId = 1;

  @override
  EntryType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return EntryType.onPremise;
      case 1:
        return EntryType.homeOffice;
      case 2:
        return EntryType.vacation;
      case 3:
        return EntryType.nonPaid;
      case 4:
        return EntryType.sickness;
      case 5:
        return EntryType.holiday;
      default:
        return EntryType.onPremise;
    }
  }

  @override
  void write(BinaryWriter writer, EntryType obj) {
    switch (obj) {
      case EntryType.onPremise:
        writer.writeByte(0);
        break;
      case EntryType.homeOffice:
        writer.writeByte(1);
        break;
      case EntryType.vacation:
        writer.writeByte(2);
        break;
      case EntryType.nonPaid:
        writer.writeByte(3);
        break;
      case EntryType.sickness:
        writer.writeByte(4);
        break;
      case EntryType.holiday:
        writer.writeByte(5);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is EntryTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
