import 'package:hive/hive.dart';

part 'entry_type.g.dart';

@HiveType(typeId: 1)
enum EntryType {
  @HiveField(0)
  onPremise,

  @HiveField(1)
  homeOffice,

  @HiveField(2)
  vacation,

  @HiveField(3)
  nonPaid,

  @HiveField(4)
  sickness,

  @HiveField(5)
  holiday
}
