import 'package:hive/hive.dart';

part 'user_type.g.dart';

@HiveType(typeId: 5)
enum UserType {
  @HiveField(0)
  guest,

  @HiveField(1)
  registered
}
