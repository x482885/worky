import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:worky/models/base_entity.dart';
import 'package:worky/models/enums/check_period.dart';
import 'package:worky/models/schedule.dart';

part 'work_preferences.g.dart';

@JsonSerializable()
@HiveType(typeId: 6)
@CopyWith()
class WorkPreferences extends BaseEntity {
  @HiveField(3)
  final String? companyName;
  @HiveField(4)
  final double? monthlySalary;
  @HiveField(5)
  final double? taxRate;
  @HiveField(6)
  final int? availableHolidayDays;
  @HiveField(7)
  final int? availableSickDays;
  @HiveField(8)
  final int? paydayDay;
  @HiveField(9)
  final CheckPeriod? checkPeriod;
  @HiveField(10)
  final double? ratio;
  @HiveField(11)
  final Schedule schedule;

  WorkPreferences(
      {super.createdAt,
      this.companyName,
      this.monthlySalary,
      this.taxRate,
      this.availableHolidayDays,
      this.availableSickDays,
      this.paydayDay,
      this.checkPeriod,
      this.ratio,
      required this.schedule});

  factory WorkPreferences.fromJson(Map<String, dynamic> json) =>
      _$WorkPreferencesFromJson(json);

  Map<String, dynamic> toJson() => _$WorkPreferencesToJson(this);
}
