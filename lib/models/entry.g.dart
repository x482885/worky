// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entry.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$EntryCWProxy {
  Entry createdAt(DateTime? createdAt);

  Entry entryType(EntryType entryType);

  Entry start(DateTime start);

  Entry end(DateTime end);

  Entry comment(String? comment);

  Entry userId(int userId);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `Entry(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// Entry(...).copyWith(id: 12, name: "My name")
  /// ````
  Entry call({
    DateTime? createdAt,
    EntryType? entryType,
    DateTime? start,
    DateTime? end,
    String? comment,
    int? userId,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfEntry.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfEntry.copyWith.fieldName(...)`
class _$EntryCWProxyImpl implements _$EntryCWProxy {
  const _$EntryCWProxyImpl(this._value);

  final Entry _value;

  @override
  Entry createdAt(DateTime? createdAt) => this(createdAt: createdAt);

  @override
  Entry entryType(EntryType entryType) => this(entryType: entryType);

  @override
  Entry start(DateTime start) => this(start: start);

  @override
  Entry end(DateTime end) => this(end: end);

  @override
  Entry comment(String? comment) => this(comment: comment);

  @override
  Entry userId(int userId) => this(userId: userId);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `Entry(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// Entry(...).copyWith(id: 12, name: "My name")
  /// ````
  Entry call({
    Object? createdAt = const $CopyWithPlaceholder(),
    Object? entryType = const $CopyWithPlaceholder(),
    Object? start = const $CopyWithPlaceholder(),
    Object? end = const $CopyWithPlaceholder(),
    Object? comment = const $CopyWithPlaceholder(),
    Object? userId = const $CopyWithPlaceholder(),
  }) {
    return Entry(
      createdAt: createdAt == const $CopyWithPlaceholder()
          ? _value.createdAt
          // ignore: cast_nullable_to_non_nullable
          : createdAt as DateTime?,
      entryType: entryType == const $CopyWithPlaceholder() || entryType == null
          ? _value.entryType
          // ignore: cast_nullable_to_non_nullable
          : entryType as EntryType,
      start: start == const $CopyWithPlaceholder() || start == null
          ? _value.start
          // ignore: cast_nullable_to_non_nullable
          : start as DateTime,
      end: end == const $CopyWithPlaceholder() || end == null
          ? _value.end
          // ignore: cast_nullable_to_non_nullable
          : end as DateTime,
      comment: comment == const $CopyWithPlaceholder()
          ? _value.comment
          // ignore: cast_nullable_to_non_nullable
          : comment as String?,
      userId: userId == const $CopyWithPlaceholder() || userId == null
          ? _value.userId
          // ignore: cast_nullable_to_non_nullable
          : userId as int,
    );
  }
}

extension $EntryCopyWith on Entry {
  /// Returns a callable class that can be used as follows: `instanceOfEntry.copyWith(...)` or like so:`instanceOfEntry.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$EntryCWProxy get copyWith => _$EntryCWProxyImpl(this);
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class EntryAdapter extends TypeAdapter<Entry> {
  @override
  final int typeId = 2;

  @override
  Entry read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Entry(
      createdAt: fields[1] as DateTime?,
      entryType: fields[3] as EntryType,
      start: fields[4] as DateTime,
      end: fields[5] as DateTime,
      comment: fields[6] as String?,
      userId: fields[7] as int,
    )
      ..id = fields[0] as int?
      ..modifiedAt = fields[2] as DateTime;
  }

  @override
  void write(BinaryWriter writer, Entry obj) {
    writer
      ..writeByte(8)
      ..writeByte(3)
      ..write(obj.entryType)
      ..writeByte(4)
      ..write(obj.start)
      ..writeByte(5)
      ..write(obj.end)
      ..writeByte(6)
      ..write(obj.comment)
      ..writeByte(7)
      ..write(obj.userId)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.createdAt)
      ..writeByte(2)
      ..write(obj.modifiedAt);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is EntryAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Entry _$EntryFromJson(Map<String, dynamic> json) => Entry(
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      entryType: $enumDecode(_$EntryTypeEnumMap, json['entryType']),
      start: DateTime.parse(json['start'] as String),
      end: DateTime.parse(json['end'] as String),
      comment: json['comment'] as String?,
      userId: (json['userId'] as num).toInt(),
    )
      ..id = (json['id'] as num?)?.toInt()
      ..modifiedAt = DateTime.parse(json['modifiedAt'] as String);

Map<String, dynamic> _$EntryToJson(Entry instance) => <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt.toIso8601String(),
      'modifiedAt': instance.modifiedAt.toIso8601String(),
      'entryType': _$EntryTypeEnumMap[instance.entryType]!,
      'start': instance.start.toIso8601String(),
      'end': instance.end.toIso8601String(),
      'comment': instance.comment,
      'userId': instance.userId,
    };

const _$EntryTypeEnumMap = {
  EntryType.onPremise: 'onPremise',
  EntryType.homeOffice: 'homeOffice',
  EntryType.vacation: 'vacation',
  EntryType.nonPaid: 'nonPaid',
  EntryType.sickness: 'sickness',
  EntryType.holiday: 'holiday',
};
