import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:hive/hive.dart';

abstract class BaseEntity {
  @HiveField(0)
  @CopyWithField(immutable: true)
  int? id;

  @HiveField(1)
  final DateTime createdAt;

  @HiveField(2)
  DateTime modifiedAt;

  BaseEntity({this.id, DateTime? createdAt, DateTime? modifiedAt})
      : createdAt = createdAt ?? DateTime.now(),
        modifiedAt = modifiedAt ?? DateTime.now();
}
