// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$ScheduleCWProxy {
  Schedule hoursPerWeek(int? hoursPerWeek);

  Schedule isMonthlyBasis(bool isMonthlyBasis);

  Schedule workingDays(List<bool>? workingDays);

  Schedule workStart(TimeOfDay workStart);

  Schedule workEnd(TimeOfDay workEnd);

  Schedule deductedMealTime(Duration? deductedMealTime);

  Schedule hoursAfterMealTimeDeducted(Duration? hoursAfterMealTimeDeducted);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `Schedule(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// Schedule(...).copyWith(id: 12, name: "My name")
  /// ````
  Schedule call({
    int? hoursPerWeek,
    bool? isMonthlyBasis,
    List<bool>? workingDays,
    TimeOfDay? workStart,
    TimeOfDay? workEnd,
    Duration? deductedMealTime,
    Duration? hoursAfterMealTimeDeducted,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfSchedule.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfSchedule.copyWith.fieldName(...)`
class _$ScheduleCWProxyImpl implements _$ScheduleCWProxy {
  const _$ScheduleCWProxyImpl(this._value);

  final Schedule _value;

  @override
  Schedule hoursPerWeek(int? hoursPerWeek) => this(hoursPerWeek: hoursPerWeek);

  @override
  Schedule isMonthlyBasis(bool isMonthlyBasis) =>
      this(isMonthlyBasis: isMonthlyBasis);

  @override
  Schedule workingDays(List<bool>? workingDays) =>
      this(workingDays: workingDays);

  @override
  Schedule workStart(TimeOfDay workStart) => this(workStart: workStart);

  @override
  Schedule workEnd(TimeOfDay workEnd) => this(workEnd: workEnd);

  @override
  Schedule deductedMealTime(Duration? deductedMealTime) =>
      this(deductedMealTime: deductedMealTime);

  @override
  Schedule hoursAfterMealTimeDeducted(Duration? hoursAfterMealTimeDeducted) =>
      this(hoursAfterMealTimeDeducted: hoursAfterMealTimeDeducted);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `Schedule(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// Schedule(...).copyWith(id: 12, name: "My name")
  /// ````
  Schedule call({
    Object? hoursPerWeek = const $CopyWithPlaceholder(),
    Object? isMonthlyBasis = const $CopyWithPlaceholder(),
    Object? workingDays = const $CopyWithPlaceholder(),
    Object? workStart = const $CopyWithPlaceholder(),
    Object? workEnd = const $CopyWithPlaceholder(),
    Object? deductedMealTime = const $CopyWithPlaceholder(),
    Object? hoursAfterMealTimeDeducted = const $CopyWithPlaceholder(),
  }) {
    return Schedule(
      hoursPerWeek: hoursPerWeek == const $CopyWithPlaceholder()
          ? _value.hoursPerWeek
          // ignore: cast_nullable_to_non_nullable
          : hoursPerWeek as int?,
      isMonthlyBasis: isMonthlyBasis == const $CopyWithPlaceholder() ||
              isMonthlyBasis == null
          ? _value.isMonthlyBasis
          // ignore: cast_nullable_to_non_nullable
          : isMonthlyBasis as bool,
      workingDays: workingDays == const $CopyWithPlaceholder()
          ? _value.workingDays
          // ignore: cast_nullable_to_non_nullable
          : workingDays as List<bool>?,
      workStart: workStart == const $CopyWithPlaceholder() || workStart == null
          ? _value.workStart
          // ignore: cast_nullable_to_non_nullable
          : workStart as TimeOfDay,
      workEnd: workEnd == const $CopyWithPlaceholder() || workEnd == null
          ? _value.workEnd
          // ignore: cast_nullable_to_non_nullable
          : workEnd as TimeOfDay,
      deductedMealTime: deductedMealTime == const $CopyWithPlaceholder()
          ? _value.deductedMealTime
          // ignore: cast_nullable_to_non_nullable
          : deductedMealTime as Duration?,
      hoursAfterMealTimeDeducted:
          hoursAfterMealTimeDeducted == const $CopyWithPlaceholder()
              ? _value.hoursAfterMealTimeDeducted
              // ignore: cast_nullable_to_non_nullable
              : hoursAfterMealTimeDeducted as Duration?,
    );
  }
}

extension $ScheduleCopyWith on Schedule {
  /// Returns a callable class that can be used as follows: `instanceOfSchedule.copyWith(...)` or like so:`instanceOfSchedule.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$ScheduleCWProxy get copyWith => _$ScheduleCWProxyImpl(this);
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ScheduleAdapter extends TypeAdapter<Schedule> {
  @override
  final int typeId = 8;

  @override
  Schedule read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Schedule(
      hoursPerWeek: fields[0] as int?,
      isMonthlyBasis: fields[1] as bool,
      workingDays: (fields[2] as List?)?.cast<bool>(),
      workStart: fields[3] as TimeOfDay,
      workEnd: fields[4] as TimeOfDay,
      deductedMealTime: fields[5] as Duration?,
      hoursAfterMealTimeDeducted: fields[6] as Duration?,
    );
  }

  @override
  void write(BinaryWriter writer, Schedule obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.hoursPerWeek)
      ..writeByte(1)
      ..write(obj.isMonthlyBasis)
      ..writeByte(2)
      ..write(obj.workingDays)
      ..writeByte(3)
      ..write(obj.workStart)
      ..writeByte(4)
      ..write(obj.workEnd)
      ..writeByte(5)
      ..write(obj.deductedMealTime)
      ..writeByte(6)
      ..write(obj.hoursAfterMealTimeDeducted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ScheduleAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Schedule _$ScheduleFromJson(Map<String, dynamic> json) => Schedule(
      hoursPerWeek: (json['hoursPerWeek'] as num?)?.toInt(),
      isMonthlyBasis: json['isMonthlyBasis'] as bool? ?? true,
      workingDays: (json['workingDays'] as List<dynamic>?)
          ?.map((e) => e as bool)
          .toList(),
      workStart: json['workStart'] == null
          ? const TimeOfDay(hour: 8, minute: 0)
          : _timeOfDayFromJson(json['workStart'] as String),
      workEnd: json['workEnd'] == null
          ? const TimeOfDay(hour: 16, minute: 30)
          : _timeOfDayFromJson(json['workEnd'] as String),
      deductedMealTime: json['deductedMealTime'] == null
          ? null
          : Duration(microseconds: (json['deductedMealTime'] as num).toInt()),
      hoursAfterMealTimeDeducted: json['hoursAfterMealTimeDeducted'] == null
          ? null
          : Duration(
              microseconds:
                  (json['hoursAfterMealTimeDeducted'] as num).toInt()),
    );

Map<String, dynamic> _$ScheduleToJson(Schedule instance) => <String, dynamic>{
      'hoursPerWeek': instance.hoursPerWeek,
      'isMonthlyBasis': instance.isMonthlyBasis,
      'workingDays': instance.workingDays,
      'workStart': _timeOfDayToJson(instance.workStart),
      'workEnd': _timeOfDayToJson(instance.workEnd),
      'deductedMealTime': instance.deductedMealTime?.inMicroseconds,
      'hoursAfterMealTimeDeducted':
          instance.hoursAfterMealTimeDeducted?.inMicroseconds,
    };
