// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$UserCWProxy {
  User createdAt(DateTime? createdAt);

  User userType(UserType userType);

  User name(String name);

  User email(String email);

  User phoneNumber(double? phoneNumber);

  User workPreferences(WorkPreferences workPreferences);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `User(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// User(...).copyWith(id: 12, name: "My name")
  /// ````
  User call({
    DateTime? createdAt,
    UserType? userType,
    String? name,
    String? email,
    double? phoneNumber,
    WorkPreferences? workPreferences,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfUser.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfUser.copyWith.fieldName(...)`
class _$UserCWProxyImpl implements _$UserCWProxy {
  const _$UserCWProxyImpl(this._value);

  final User _value;

  @override
  User createdAt(DateTime? createdAt) => this(createdAt: createdAt);

  @override
  User userType(UserType userType) => this(userType: userType);

  @override
  User name(String name) => this(name: name);

  @override
  User email(String email) => this(email: email);

  @override
  User phoneNumber(double? phoneNumber) => this(phoneNumber: phoneNumber);

  @override
  User workPreferences(WorkPreferences workPreferences) =>
      this(workPreferences: workPreferences);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `User(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// User(...).copyWith(id: 12, name: "My name")
  /// ````
  User call({
    Object? createdAt = const $CopyWithPlaceholder(),
    Object? userType = const $CopyWithPlaceholder(),
    Object? name = const $CopyWithPlaceholder(),
    Object? email = const $CopyWithPlaceholder(),
    Object? phoneNumber = const $CopyWithPlaceholder(),
    Object? workPreferences = const $CopyWithPlaceholder(),
  }) {
    return User(
      createdAt: createdAt == const $CopyWithPlaceholder()
          ? _value.createdAt
          // ignore: cast_nullable_to_non_nullable
          : createdAt as DateTime?,
      userType: userType == const $CopyWithPlaceholder() || userType == null
          ? _value.userType
          // ignore: cast_nullable_to_non_nullable
          : userType as UserType,
      name: name == const $CopyWithPlaceholder() || name == null
          ? _value.name
          // ignore: cast_nullable_to_non_nullable
          : name as String,
      email: email == const $CopyWithPlaceholder() || email == null
          ? _value.email
          // ignore: cast_nullable_to_non_nullable
          : email as String,
      phoneNumber: phoneNumber == const $CopyWithPlaceholder()
          ? _value.phoneNumber
          // ignore: cast_nullable_to_non_nullable
          : phoneNumber as double?,
      workPreferences: workPreferences == const $CopyWithPlaceholder() ||
              workPreferences == null
          ? _value.workPreferences
          // ignore: cast_nullable_to_non_nullable
          : workPreferences as WorkPreferences,
    );
  }
}

extension $UserCopyWith on User {
  /// Returns a callable class that can be used as follows: `instanceOfUser.copyWith(...)` or like so:`instanceOfUser.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$UserCWProxy get copyWith => _$UserCWProxyImpl(this);
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserAdapter extends TypeAdapter<User> {
  @override
  final int typeId = 10;

  @override
  User read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return User(
      createdAt: fields[1] as DateTime?,
      userType: fields[3] as UserType,
      name: fields[4] as String,
      email: fields[5] as String,
      phoneNumber: fields[6] as double?,
      workPreferences: fields[7] as WorkPreferences,
    )
      ..id = fields[0] as int?
      ..modifiedAt = fields[2] as DateTime;
  }

  @override
  void write(BinaryWriter writer, User obj) {
    writer
      ..writeByte(8)
      ..writeByte(3)
      ..write(obj.userType)
      ..writeByte(4)
      ..write(obj.name)
      ..writeByte(5)
      ..write(obj.email)
      ..writeByte(6)
      ..write(obj.phoneNumber)
      ..writeByte(7)
      ..write(obj.workPreferences)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.createdAt)
      ..writeByte(2)
      ..write(obj.modifiedAt);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      userType: $enumDecode(_$UserTypeEnumMap, json['userType']),
      name: json['name'] as String,
      email: json['email'] as String,
      phoneNumber: (json['phoneNumber'] as num?)?.toDouble(),
      workPreferences: WorkPreferences.fromJson(
          json['workPreferences'] as Map<String, dynamic>),
    )
      ..id = (json['id'] as num?)?.toInt()
      ..modifiedAt = DateTime.parse(json['modifiedAt'] as String);

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt.toIso8601String(),
      'modifiedAt': instance.modifiedAt.toIso8601String(),
      'userType': _$UserTypeEnumMap[instance.userType]!,
      'name': instance.name,
      'email': instance.email,
      'phoneNumber': instance.phoneNumber,
      'workPreferences': instance.workPreferences,
    };

const _$UserTypeEnumMap = {
  UserType.guest: 'guest',
  UserType.registered: 'registered',
};
