import 'package:json_annotation/json_annotation.dart';
import 'package:hive/hive.dart';
import 'package:copy_with_extension/copy_with_extension.dart';

import 'enums/entry_type.dart';

import 'base_entity.dart';

part 'entry.g.dart';

@JsonSerializable()
@HiveType(typeId: 2)
@CopyWith()
class Entry extends BaseEntity {
  @HiveField(3)
  final EntryType entryType;

  @HiveField(4)
  final DateTime start;

  @HiveField(5)
  final DateTime end;

  @HiveField(6)
  final String? comment;

  @HiveField(7)
  final int userId;

  Entry(
      {super.createdAt,
      required this.entryType,
      required this.start,
      required this.end,
      this.comment,
      required this.userId});

  factory Entry.fromJson(Map<String, dynamic> json) => _$EntryFromJson(json);

  Map<String, dynamic> toJson() => _$EntryToJson(this);

  Duration get workTime => end.difference(start);
}
