import 'package:flutter/material.dart';

extension TimeOfDayExtension on TimeOfDay {
  double toDouble() => hour + minute / 60.0;
  Duration difference(TimeOfDay other) {
    return Duration(hours: hour - other.hour, minutes: minute - other.minute);
  }
}
