extension DateTimeExtension on DateTime {
  DateTime getDateWithoutTime() {
    return DateTime(year, month, day);
  }

  bool isSameDate(DateTime date2) {
    return year == date2.year && month == date2.month && day == date2.day;
  }

  bool isSameWeek(DateTime date2) {
    final thisWeekMonday =
        weekday == 1 ? this : subtract(Duration(days: weekday - 1));
    final otherWeekMonday = date2.weekday == 1
        ? date2
        : date2.subtract(Duration(days: date2.weekday - 1));

    return thisWeekMonday.isSameDate(otherWeekMonday);
  }
}
