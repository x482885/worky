extension DurationExtension on Duration {
  double toDoubleMinutes() {
    return inMinutes / 60; //minute precision
  }
}
