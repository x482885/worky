import 'package:get_it/get_it.dart';
import 'package:worky/models/entry.dart';
import 'package:worky/models/schedule.dart';
import 'package:worky/models/user.dart';
import 'package:worky/models/work_preferences.dart';
import 'package:worky/services/generic_service_offline.dart';
import 'package:worky/utils/extensions/datetime_extentions.dart';
import 'package:worky/utils/utils.dart';

class Metrics {
  final int _userId;
  final _userService = GetIt.instance.get<GenericServiceOffline<User>>();
  final _entryService = GetIt.instance.get<GenericServiceOffline<Entry>>();
  Metrics({required int userId}) : _userId = userId;

  Duration scheduledWorkTimeInMonth({DateTime? date}) {
    date ??= DateTime.now();
    if (schedule == null) return Duration.zero;

    Duration totalWorkTime = Duration.zero;

    for (DateTime day in Utils.getDaysInMonth(date: date)) {
      if (schedule!.workingDays[day.weekday - 1]) {
        totalWorkTime = Duration(
            minutes: totalWorkTime.inMinutes + schedule!.workTime.inMinutes);
      }
    }
    return totalWorkTime;
  }

  Duration workedTimeInRange({required DateTime from, required DateTime to}) {
    if (schedule == null || entries.isEmpty) return Duration.zero;

    // inclusive range
    var entriesInRange = entries.where((entry) =>
        entry.start.isAfter(from.subtract(const Duration(days: 1))) &&
        entry.start.isBefore(to.add(const Duration(days: 1))));

    Duration totalWorkTime = Duration.zero;

    for (DateTime day in Utils.getDaysInRange(from: from, to: to)) {
      Entry? entry = entriesInRange
          .where((entry) => entry.start.isSameDate(day))
          .firstOrNull;
      if (entry != null) {
        totalWorkTime = Duration(
            minutes: totalWorkTime.inMinutes + entry.workTime.inMinutes);
        totalWorkTime = _deductMealTime(totalWorkTime);
      }
    }
    return totalWorkTime;
  }

  Duration scheduledWorkTimeInWeek({DateTime? date}) {
    date ??= DateTime.now();
    if (schedule == null) return Duration.zero;

    Duration totalWorkTime = Duration.zero;

    for (DateTime day in Utils.getDaysInWeek(date: date)) {
      if (schedule!.workingDays[day.weekday - 1]) {
        totalWorkTime = Duration(
            minutes: totalWorkTime.inMinutes + schedule!.workTime.inMinutes);
      }
    }
    return totalWorkTime;
  }

  List<DateTime> getWorkDaysInRange(
      {required DateTime from, required DateTime to}) {
    var daysInRange = Utils.getDaysInRange(from: from, to: to);
    List<DateTime> workDaysInRange = [];
    for (DateTime day in daysInRange) {
      if (schedule!.workingDays[day.weekday - 1]) {
        workDaysInRange.add(day);
      }
    }
    return workDaysInRange;
  }

  Duration _deductMealTime(Duration time) {
    if (schedule != null &&
        schedule?.hoursAfterMealTimeDeducted != null &&
        schedule?.deductedMealTime != null &&
        time >= schedule!.hoursAfterMealTimeDeducted!) {
      time = Duration(
          minutes: time.inMinutes - schedule!.deductedMealTime!.inMinutes);
    }
    return time;
  }

  User? get user => _userService.getItem(_userId);
  WorkPreferences? get workPreferences => user?.workPreferences;
  Schedule? get schedule => workPreferences?.schedule;
  List<Entry> get entries =>
      _entryService.getItems().where((e) => e.userId == _userId).toList();
}
