import 'package:flutter/material.dart';
import 'package:worky/models/schedule.dart';
import 'package:worky/utils/extensions/datetime_extentions.dart';
import 'package:worky/utils/extensions/time_of_day_extensions.dart';

final class Utils {
  static List<DateTime> getDaysInRange({DateTime? from, required DateTime to}) {
    from ??= DateTime.now();
    final List<DateTime> daysInRange = [];
    for (int i = 0; i <= to.difference(from).inDays; i++) {
      daysInRange.add(from.add(Duration(days: i)));
    }
    return daysInRange;
  }

  static List<DateTime> getDaysInMonth({DateTime? date}) {
    date ??= DateTime.now();
    final firstDayOfMonth = DateTime(date.year, date.month, 1);
    final lastDayOfMonth = DateTime(date.year, date.month + 1, 0)
        .subtract(const Duration(days: 1));

    return getDaysInRange(from: firstDayOfMonth, to: lastDayOfMonth);
  }

  static List<DateTime> getDaysInWeek({DateTime? date}) {
    date ??= DateTime.now();
    final firstDayOfWeek = date.subtract(Duration(days: date.weekday - 1));
    final lastDayOfWeek = firstDayOfWeek.add(const Duration(days: 6));
    return getDaysInRange(from: firstDayOfWeek, to: lastDayOfWeek);
  }

  static Duration getDurationUntilNextWorkStart(Schedule schedule) {
    final currentDateTime = DateTime.now();
    final currentDate = currentDateTime.getDateWithoutTime();
    final currentTimeOfDay = TimeOfDay.fromDateTime(currentDateTime);
    int daysToAdd = 0;

    if (currentTimeOfDay.toDouble() >= schedule.workStart.toDouble() &&
        currentTimeOfDay.toDouble() < schedule.workEnd.toDouble() &&
        schedule.workingDays[(currentDate.weekday - 1)]) {
      // return negative to signal that the work has already started and not ended
      return -currentDateTime.difference(currentDate.add(Duration(
          hours: schedule.workEnd.hour, minutes: schedule.workEnd.minute)));
    }

    if (currentTimeOfDay.toDouble() > schedule.workEnd.toDouble()) {
      daysToAdd++;
    }

    // currentDate.add(Duration(days: daysToAdd)) does not modify the currentDate
    while (!schedule.workingDays[
        ((currentDate.add(Duration(days: daysToAdd))).weekday - 1)]) {
      daysToAdd++;
    }

    final nextWorkStartDateTime = currentDate
        .add(Duration(days: daysToAdd))
        .add(Duration(
            hours: schedule.workStart.hour,
            minutes: schedule.workStart.minute));
    return nextWorkStartDateTime.difference(currentDateTime);
  }
}
