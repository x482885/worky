import 'package:flutter/material.dart';

import '../widgets/notification_tile.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notifications'),
        actions: [
          TextButton(
            onPressed: () {
              // clear all notifications
            },
            child: const Text('Clear All'),
          )
        ],
      ),
      body: ListView(
        children: [
          NotificationTile(
            title: 'Notification Title',
            description: 'Notification description text',
            onClear: () {
              // clear this notification
            },
          ),
        ],
      ),
    );
  }
}
