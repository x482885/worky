import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:worky/models/entry.dart';
import 'package:worky/pages/entry_page.dart';
import 'package:worky/services/generic_service_offline.dart';
import 'package:worky/utils/extensions/duration_extensions.dart';
import 'package:worky/utils/metrics.dart';
import 'package:worky/utils/utils.dart';
import 'package:worky/widgets/coutndown_timer.dart';
import 'package:worky/widgets/schedule_week_days.dart';
import 'package:worky/widgets/titled_divider.dart';
import '../models/user.dart';

class HomePage extends StatelessWidget {
  final userService = GetIt.instance.get<GenericServiceOffline<User>>();
  final entryService = GetIt.instance.get<GenericServiceOffline<Entry>>();
  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: userService.listStream,
        builder: (context, snapshot) {
          final user = userService.getItem(0);
          return Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: const Text("Home"),
            ),
            body: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  if (user != null && user.workPreferences.companyName != null)
                    Flexible(
                      flex: 1,
                      child: Column(
                        children: [
                          const Text("Company:"),
                          Text(
                            user.workPreferences.companyName!,
                            style: const TextStyle(fontSize: 30),
                          ),
                        ],
                      ),
                    ),
                  Flexible(flex: 1, child: buildWorkStartsTimer(user)),
                  if (user != null)
                    Flexible(
                      flex: 1,
                      child: ScheduleWeekDays(
                          inputWorkingDays:
                              user.workPreferences.schedule.workingDays,
                          onValueChanged: (_) {}),
                    ),
                  StreamBuilder(
                      stream: entryService.listStream,
                      builder: (context, snapshot) {
                        return Flexible(
                          flex: 3,
                          child: user != null
                              ? getMetrics(user)
                              : const Text("N/A"),
                        );
                      }),
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EntryPage(
                            selectedDate: DateTime.now(),
                            singleDayLocked: true,
                          )),
                );
              },
              child: const Icon(Icons.add),
            ),
          );
        });
  }

  Widget buildTextColumn(List content) {
    return Column(
      children: content.map((item) => Text('$item')).toList(),
    );
  }

  Widget buildWorkStartsTimer(User? user) {
    var title = const Text("Work starts in:");
    if (user == null) {
      return Column(children: [
        title,
        const Text("Work prefenreces not provided"),
      ]);
    }
    var schedule = user.workPreferences.schedule;
    var durationUntilNextWorkStart =
        Utils.getDurationUntilNextWorkStart(schedule);
    if (durationUntilNextWorkStart.isNegative) {
      title = const Text("Work ends in:");
    }
    return Column(children: [
      title,
      CountdownTimer(duration: durationUntilNextWorkStart),
    ]);
  }

  Widget getMetrics(User user) {
    var metrics = Metrics(userId: 0);
    var workTimeDuration = user.workPreferences.schedule.workTime.inHours;
    var workTimeInMonth = metrics.scheduledWorkTimeInMonth().toDoubleMinutes();
    var workedTimeInMonth = metrics
        .workedTimeInRange(
            from: Utils.getDaysInMonth().first, to: Utils.getDaysInMonth().last)
        .toDoubleMinutes();
    var workTimeInWeek = metrics.scheduledWorkTimeInWeek().toDoubleMinutes();
    var workedTimeInWeek = metrics
        .workedTimeInRange(
            from: Utils.getDaysInWeek().first, to: Utils.getDaysInWeek().last)
        .toDoubleMinutes();
    var leftWorkDaysInMonth = metrics
        .getWorkDaysInRange(
            from: DateTime.now(), to: Utils.getDaysInMonth().last)
        .where((day) => day.isAfter(DateTime.now()))
        .length;
    var leftWorkDaysInWeek = metrics
        .getWorkDaysInRange(
            from: DateTime.now(), to: Utils.getDaysInWeek().last)
        .where((day) => day.isAfter(DateTime.now()))
        .length;

    return Column(
      children: [
        const TitledDivider(title: "Core"),
        _buildRowDoubleTitledColumns(
            "Daily",
            workTimeDuration.toString(),
            "Core hours",
            "${user.workPreferences.schedule.workStart.hour} - ${user.workPreferences.schedule.workEnd.hour}"),
        const TitledDivider(title: "Monthly"),
        _buildGoalDaysLeftRowColumns("$workedTimeInMonth/$workTimeInMonth",
            leftWorkDaysInMonth.toString()),
        const TitledDivider(title: "Weekly"),
        _buildGoalDaysLeftRowColumns(
            "$workedTimeInWeek/$workTimeInWeek", leftWorkDaysInWeek.toString()),
      ],
    );
  }

  Widget _buildRowDoubleTitledColumns(
      String title1, String text1, String title2, String text2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildTitledColumn(title1, text1),
        _buildTitledColumn(title2, text2),
      ],
    );
  }

  Widget _buildGoalDaysLeftRowColumns(String text1, String text2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildTitledColumn("Goal", text1),
        _buildTitledColumn("Days left", text2),
      ],
    );
  }

  Widget _buildTitledColumn(String title, String text) {
    return Column(
      children: [
        Text(
          title,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(text),
      ],
    );
  }
}
