import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:worky/models/user.dart';
import 'package:worky/models/work_preferences.dart';
import 'package:worky/services/generic_service_offline.dart';
import 'package:worky/widgets/schedule_form.dart';
import 'package:worky/widgets/titled_divider.dart';
import 'package:worky/widgets/work_preferences_form.dart';

class WorkPreferencesPage extends StatefulWidget {
  final userService = GetIt.instance.get<GenericServiceOffline<User>>();
  WorkPreferencesPage({super.key});

  @override
  _WorkPreferencesPageState createState() => _WorkPreferencesPageState();
}

class _WorkPreferencesPageState extends State<WorkPreferencesPage> {
  bool _showSaveButton = false;
  User? user;

  @override
  void initState() {
    super.initState();
    user = widget.userService.getItem(0); //Fixed guest
  }

  @override
  Widget build(BuildContext context) {
    user = widget.userService.getItem(0);
    var workPreferencesForm = WorkPreferencesForm(
      workPreferences: user!.workPreferences,
      isEditable: _showSaveButton,
    );
    var scheduleForm = ScheduleForm(
      schedule: user!.workPreferences.schedule,
      isEditable: _showSaveButton,
    );
    return Scaffold(
      appBar: AppBar(
        title: const Text('Work Preferences'),
        actions: [
          if (_showSaveButton)
            IconButton(
              icon: const Icon(Icons.save),
              onPressed: () {
                widget.userService.update(
                    user!.id!,
                    user!.copyWith(
                        workPreferences: workPreferencesForm
                            .getWorkPreferences()
                            .copyWith(schedule: scheduleForm.getSchedule())));
                setState(() {
                  _showSaveButton = false;
                });
              },
            ),
          if (!_showSaveButton)
            TextButton(
              onPressed: () {
                setState(() {
                  _showSaveButton = true;
                });
              },
              child: const Text('edit'),
            )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(children: [
          const TitledDivider(
            title: "Work Preferences",
          ),
          workPreferencesForm,
          const TitledDivider(
            title: "Schedule",
          ),
          scheduleForm,
        ]),
      ),
    );
  }
}
