import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:worky/models/schedule.dart';
import 'package:worky/widgets/monthly_basis_dropdown.dart';
import 'package:worky/widgets/schedule_week_days.dart';
import 'package:worky/widgets/time_selector_viewer.dart';

class SchedulePage extends StatefulWidget {
  final Schedule schedule;
  final ValueChanged<Schedule> onSave;
  const SchedulePage({super.key, required this.schedule, required this.onSave});

  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage> {
  final TextEditingController _hoursPerWeekController = TextEditingController();
  bool isMonthlyBasis = true;
  final List<bool> _workingDays =
      List<bool>.generate(7, (index) => index < 5, growable: false);
  final TextEditingController _deductedMealTimeController =
      TextEditingController();
  final TextEditingController _hoursAfterMealTimeDeductedController =
      TextEditingController();
  TimeOfDay workStart = const TimeOfDay(hour: 8, minute: 0);
  TimeOfDay workEnd = const TimeOfDay(hour: 16, minute: 30);
  List<bool> workingDays = [];

  @override
  void initState() {
    super.initState();
    _hoursPerWeekController.text =
        widget.schedule.hoursPerWeek?.toString() ?? "";
    isMonthlyBasis = widget.schedule.isMonthlyBasis;
    workStart = widget.schedule.workStart;
    workEnd = widget.schedule.workEnd;
    _deductedMealTimeController.text =
        widget.schedule.deductedMealTime?.inMinutes.toString() ?? "";
    _hoursAfterMealTimeDeductedController.text =
        widget.schedule.hoursAfterMealTimeDeducted?.inMinutes.toString() ?? "";
    workingDays = widget.schedule.workingDays;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Schedule'),
        actions: [
          IconButton(
            icon: const Icon(Icons.check),
            onPressed: () {
              final newSchedule = Schedule(
                hoursPerWeek: int.tryParse(_hoursPerWeekController.text),
                isMonthlyBasis: isMonthlyBasis,
                workingDays: _workingDays,
                workStart: workStart,
                workEnd: workEnd,
                deductedMealTime: Duration(
                    minutes:
                        int.tryParse(_deductedMealTimeController.text) ?? 0),
                hoursAfterMealTimeDeducted: Duration(
                    hours: int.tryParse(
                            _hoursAfterMealTimeDeductedController.text) ??
                        0),
              );
              widget.onSave(newSchedule);
              Navigator.pop(context);
            },
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _hoursPerWeekController,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: const InputDecoration(
                labelText: 'Hours Per Week',
              ),
            ),
            MonthlyBasisDropdown(
                isMonthlyBasis: isMonthlyBasis,
                onValueChanged: (value) => isMonthlyBasis = value),
            ScheduleWeekDays(
              inputWorkingDays: workingDays,
              onValueChanged: (value) => workingDays = value,
              editable: true,
            ),
            TimeSelectorViewer(
                label: "Work start",
                inputTime: workStart,
                onTimeChanged: (time) => workStart = time),
            TimeSelectorViewer(
                label: "Work end",
                inputTime: workEnd,
                onTimeChanged: (time) => workEnd = time),
            TextField(
              controller: _deductedMealTimeController,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: const InputDecoration(
                labelText: 'Deducted Meal Time (minutes)',
              ),
            ),
            TextField(
              controller: _hoursAfterMealTimeDeductedController,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: const InputDecoration(
                labelText: 'Time after which meal time is deducted (hours)',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
