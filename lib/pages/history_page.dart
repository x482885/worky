import 'package:flutter/material.dart';

import 'package:get_it/get_it.dart';
import 'package:worky/services/generic_service_offline.dart';

import '../models/event.dart';

class HistoryPage extends StatelessWidget {
  final eventService = GetIt.instance.get<GenericServiceOffline<Event>>();
  HistoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: eventService.listStream,
        builder: (context, snapshot) {
          final events = snapshot.data ?? eventService.getItems();
          events.retainWhere(
            (element) => !element.isSeen,
          );
          return Scaffold(
            appBar: AppBar(
              title: const Text('History'),
              actions: [
                TextButton(
                  onPressed: () {
                    for (var event in events) {
                      eventService.update(
                          event.id!, event.copyWith(isSeen: true));
                    }
                  },
                  child: const Text('Clear All'),
                )
              ],
            ),
            body: events.isNotEmpty
                ? Expanded(
                    child: ListView.builder(itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(events[index].title),
                      subtitle: Text(events[index].description),
                    );
                  }))
                : null,
          );
        });
  }
}
