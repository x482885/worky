import 'package:flutter/material.dart';
import 'package:worky/widgets/calendar_calendar_page.dart';
import 'package:worky/widgets/calendar_tile_switch.dart';
import 'package:worky/widgets/calendar_tiles_page.dart';
import 'package:worky/widgets/month_selector.dart';

class CalendarPage extends StatefulWidget {
  const CalendarPage({super.key});

  @override
  State<CalendarPage> createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  bool isTileSwitched = false;
  DateTime? selectedMonthDate;
  @override
  Widget build(BuildContext context) {
    selectedMonthDate ??= DateTime.now();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Calendar"),
        actions: [
          CalendarTileSwitch(
            isTileSwitched: isTileSwitched,
            onValueChanged: (val) => setState(() {
              isTileSwitched = val;
            }),
          )
        ],
      ),
      body: Center(
        child: Column(
          children: [
            MonthSelector(
                currentMonthDate: selectedMonthDate,
                onValueChanged: (date) =>
                    setState(() => selectedMonthDate = date)),
            GestureDetector(
              onHorizontalDragEnd: (details) {
                if (details.primaryVelocity == null) return;
                // Swiping in right direction.
                setState(() {
                  if (details.primaryVelocity! > 0) {
                    selectedMonthDate = DateTime(selectedMonthDate!.year,
                        selectedMonthDate!.month - 1, selectedMonthDate!.day);
                  }

                  // Swiping in left direction.
                  if (details.primaryVelocity! < 0) {
                    selectedMonthDate = DateTime(selectedMonthDate!.year,
                        selectedMonthDate!.month + 1, selectedMonthDate!.day);
                  }
                });
              },
              child: isTileSwitched
                  ? CalendarTilesPage(
                      initialMonthDate: selectedMonthDate,
                    )
                  : CalendarCalendarPage(
                      initialMonthDate: selectedMonthDate,
                    ),
            )
          ],
        ),
      ),
    );
  }
}
