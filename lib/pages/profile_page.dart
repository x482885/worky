import 'package:flutter/material.dart';
import 'package:worky/pages/settings_page.dart';
import 'package:worky/pages/work_preferences_page.dart';
import 'history_page.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Flexible(
                flex: 1,
                child: Text(
                  "Guest user",
                  style: TextStyle(fontSize: 30),
                ),
              ),
              Expanded(
                flex: 4,
                child: Column(
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            minimumSize: const Size(200, 40)),
                        onPressed: null,
                        child: const Text('Statistics')),
                    const SizedBox(
                      height: 10,
                    ),
                    buildElevatedButton(context, 'History', HistoryPage()),
                    const SizedBox(
                      height: 10,
                    ),
                    buildElevatedButton(
                        context, 'Work Preferences', WorkPreferencesPage()),
                    const SizedBox(
                      height: 10,
                    ),
                    buildElevatedButton(
                        context, 'Settings', const SettingsPage()),
                  ],
                ),
              ),
              const Flexible(
                  flex: 1,
                  child: TextButton(
                    onPressed: null,
                    child: Text(
                      "Login",
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildElevatedButton(BuildContext context, String text, Widget page) {
    return ElevatedButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => page),
        );
      },
      style: ElevatedButton.styleFrom(minimumSize: const Size(200, 40)),
      child: Text(text),
    );
  }
}
