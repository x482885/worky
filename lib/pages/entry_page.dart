import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:get_it/get_it.dart';
import 'package:worky/models/user.dart';
import 'package:worky/services/generic_service_offline.dart';
import 'package:worky/utils/extensions/datetime_extentions.dart';
import 'package:worky/utils/extensions/time_of_day_extensions.dart';

import '../models/entry.dart';
import '../models/enums/entry_type.dart';

class EntryPage extends StatefulWidget {
  final entryService = GetIt.instance.get<GenericServiceOffline<Entry>>();
  final userService = GetIt.instance.get<GenericServiceOffline<User>>();
  final DateTime? selectedDate;
  final bool singleDayLocked;

  EntryPage({super.key, this.selectedDate, this.singleDayLocked = false});

  @override
  State<EntryPage> createState() => _EntryPageState();
}

class _EntryPageState extends State<EntryPage> {
  Entry? entry;
  DateTime date = DateTime.now();
  TimeOfDay startTime = const TimeOfDay(hour: 8, minute: 0);
  TimeOfDay endTime = const TimeOfDay(hour: 16, minute: 30);
  EntryType entryType = EntryType.onPremise;
  String? comment;
  TextEditingController commentController = TextEditingController();
  bool update = false;

  bool selectedDayComboBox = true;
  List<DateTime> selectedDates = [];

  @override
  void initState() {
    super.initState();
    if (widget.selectedDate != null) {
      date = widget.selectedDate!;
    }
    var user = widget.userService.getItem(0);
    entry = getExistingEntry(date: date);
    if (entry != null) {
      update = true;
      startTime = TimeOfDay.fromDateTime(entry!.start);
      endTime = TimeOfDay.fromDateTime(entry!.end);
      entryType = entry!.entryType;
      comment = entry!.comment;
    } else if (user != null) {
      // load from schedule
      startTime = user.workPreferences.schedule.workStart;
      endTime = user.workPreferences.schedule.workEnd;
    }
    if (comment != null) {
      commentController.text = comment!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: update ? const Text('Edit Entry') : const Text('Create Entry'),
        actions: [
          IconButton(
            icon: const Icon(Icons.check),
            onPressed: () {
              selectedDates.isEmpty ? saveSingleEntry() : saveMultipleEntries();
              Navigator.pop(context);
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: Row(
                children: [
                  DropdownButton(
                    value: widget.singleDayLocked ? true : selectedDayComboBox,
                    items: const [
                      DropdownMenuItem(value: true, child: Text('Day')),
                      DropdownMenuItem(value: false, child: Text('Multiple')),
                    ],
                    onChanged: widget.singleDayLocked
                        ? null
                        : (value) {
                            setState(() {
                              selectedDayComboBox = value as bool;
                            });
                          },
                  ),
                ],
              ),
              trailing: TextButton(
                  onPressed: widget.singleDayLocked
                      ? null
                      : () async {
                          if (selectedDayComboBox) {
                            selectedDates.clear();
                          }
                          await showCalendar(context, selectedDayComboBox);
                        },
                  child: selectedDates.isEmpty
                      ? Text(DateFormat('EEEE, M/d/y').format(date))
                      : Text(
                          "${DateFormat('d/M').format(selectedDates.first)} - ${DateFormat('d/M').format(selectedDates.last)}")),
            ),
            ListTile(
              title: const Text('Start Time'),
              trailing: Text(startTime.format(context)),
              onTap: () async {
                final time = await showTimePicker(
                  context: context,
                  initialTime: startTime,
                );

                if (time != null) {
                  setState(() {
                    startTime = time;
                    if (endTime.toDouble() <= startTime.toDouble()) {
                      endTime = time.replacing(hour: time.hour + 1);
                    }
                  });
                }
              },
            ),
            ListTile(
              title: const Text('End Time'),
              trailing: Text(endTime.format(context)),
              onTap: () async {
                final time = await showTimePicker(
                  context: context,
                  initialTime: endTime,
                );

                if (time != null) {
                  if (time.toDouble() <= startTime.toDouble()) {
                    setState(() {
                      endTime = startTime.replacing(hour: startTime.hour + 1);
                    });
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                              title: const Text('Error'),
                              content: const Text(
                                  'End time must be greater than start time.'),
                              actions: [
                                TextButton(
                                    child: const Text('OK'),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    })
                              ]);
                        });
                  } else {
                    endTime = time;
                  }
                }
              },
            ),
            ListTile(
              title: const Text('Entry type'),
              trailing: DropdownButton<EntryType>(
                value: entryType,
                items: EntryType.values.map((EntryType type) {
                  return DropdownMenuItem<EntryType>(
                    value: type,
                    child: Text(
                      type.name,
                    ),
                  );
                }).toList(),
                onChanged: (EntryType? newValue) {
                  setState(() {
                    entryType = newValue!;
                  });
                },
              ),
            ),
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    const Text('Comment'),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey,
                          width: 1,
                        ),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: TextField(
                        maxLines: 4,
                        controller: commentController,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                )),
            const SizedBox(height: 30),
            if (entry != null)
              Center(
                child: TextButton(
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.red,
                    side: const BorderSide(color: Colors.red),
                  ),
                  onPressed: () async {
                    await showDeleteAlert();
                    Navigator.pop(context);
                  },
                  child: const Text("Delete"),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<void> showDeleteAlert() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text("Confirm Delete"),
        content: const Text("Are you sure you want to delete this entry?"),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("Cancel"),
          ),
          TextButton(
            onPressed: () async {
              await widget.entryService.delete(entry!.id!);
              Navigator.pop(context);
            },
            child: const Text("Delete"),
          ),
        ],
      ),
    );
  }

  Future<void> showCalendar(BuildContext inputContext, bool single) async {
    await showDialog(
        context: inputContext,
        builder: (BuildContext dialogContext) {
          return Center(
            child: SizedBox(
              width: 300,
              height: 300,
              child: SfDateRangePicker(
                showActionButtons: true,
                initialDisplayDate: single ? date : null,
                initialSelectedDates: single ? null : selectedDates,
                selectionMode: single
                    ? DateRangePickerSelectionMode.single
                    : DateRangePickerSelectionMode.multiple,
                initialSelectedRange: PickerDateRange(
                  DateTime(DateTime.now().year - 1),
                  DateTime(DateTime.now().year + 1),
                ),
                onSubmit: (value) async {
                  if (value is List<DateTime>) {
                    var entryExists =
                        value.any((e) => getExistingEntry(date: e) != null);
                    if (entryExists &&
                        (await showEntryOverwriteDialog()) != true) {
                      Navigator.pop(dialogContext);
                      return;
                    }
                    setState(() {
                      update = entryExists;
                      selectedDates = value;
                      selectedDates.sort();
                    });
                  } else if (value is DateTime) {
                    var existingEntry = getExistingEntry(date: value);
                    if (!date.isSameDate(value) &&
                        existingEntry != null &&
                        (await showEntryOverwriteDialog()) != true) {
                      Navigator.pop(dialogContext);
                      return;
                    }
                    setState(() {
                      if (existingEntry != null) {
                        entry = existingEntry;
                        update = true;
                      } else {
                        update = false;
                      }
                      date = value;
                      selectedDates.clear();
                    });
                  }
                  Navigator.pop(dialogContext);
                },
                onCancel: () => Navigator.pop(dialogContext),
              ),
            ),
          );
        });
  }

  void showEntryForDayExistsDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Entry Already Exists'),
        content: const Text('There already exists an entry for this date.'),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  Future<bool?> showEntryOverwriteDialog() async {
    return showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Entry Overwrite'),
        content: const Text(
            'There already exists an entry for this date, are you sure you want to overwrite it?'),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context, false),
            child: const Text('No'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, true),
            child: const Text('Yes'),
          ),
        ],
      ),
    );
  }

  void saveSingleEntry() {
    final Entry newEntry = getNewEntry(date: date);
    update
        ? widget.entryService.update(entry!.id!, newEntry)
        : widget.entryService.create(newEntry);
  }

  void saveMultipleEntries() {
    List<Entry> newEntries = [];
    Map<int, Entry> updatedEntries = {};
    for (final DateTime d in selectedDates) {
      Entry? existingEntry = getExistingEntry(date: d);
      Entry newEntry = getNewEntry(date: d);

      if (existingEntry != null) {
        newEntry.id = existingEntry.id!;
        updatedEntries[newEntry.id!] = newEntry;
      } else {
        newEntries.add(newEntry);
      }
    }
    if (newEntries.isNotEmpty) {
      widget.entryService.createAll(newEntries);
    }
    if (updatedEntries.isNotEmpty) {
      widget.entryService.updateAll(updatedEntries);
    }
  }

  Entry getNewEntry({required DateTime date}) {
    final Entry newEntry = Entry(
        createdAt: DateTime.now(),
        entryType: entryType,
        start: DateTime(
            date.year, date.month, date.day, startTime.hour, startTime.minute),
        end: DateTime(
            date.year, date.month, date.day, endTime.hour, endTime.minute),
        comment: commentController.text,
        userId: entry?.id ?? 0 //perma guest user
        );
    return newEntry;
  }

  Entry? getExistingEntry({required DateTime date}) => widget.entryService
      .getItems()
      .where((Entry e) =>
          e.start.isSameDate(date) && e.userId == 0) // perma guest user
      .firstOrNull;
}
