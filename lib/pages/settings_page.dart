import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Setings"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: [
            Row(
              children: [
                const Text('Profile Settings'),
                const Spacer(),
                ElevatedButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: const Text('Guest User'),
                          content:
                              const Text('You are currently a guest user.'),
                          actions: [
                            TextButton(
                              onPressed: () => Navigator.pop(context),
                              child: const Text('OK'),
                            ),
                          ],
                        ),
                      );
                    },
                    child: const Text('Settings')),
              ],
            ),
            const Divider(height: 32),
            const Text('System', style: TextStyle(fontSize: 18)),
            Row(
              children: [
                const Text('Currency'),
                const Spacer(),
                TextButton(
                  onPressed: () => showCurrencyPicker(
                    context: context,
                    showFlag: false,
                    showCurrencyName: true,
                    showCurrencyCode: true,
                    onSelect: (Currency currency) {
                      // TODO save the currency setting
                    },
                  ),
                  child: const Text("Set.."),
                ), //TODO: get current currency
              ],
            ),
            //const Divider(height: 32),
            //const Text('Notifications', style: TextStyle(fontSize: 18)),
            // TODO add neccesary check boxes
          ],
        ),
      ),
    );
  }
}
